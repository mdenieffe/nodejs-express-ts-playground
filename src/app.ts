import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as path from 'path';

import {WeatherService} from './services/WeatherService';

import cookieParser = require('cookie-parser');
import {Request, Response, NextFunction} from "express"; // this module doesn't use the ES6 default export yet

const app: express.Express = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', index);
// app.use('/users', users);
app.use('/city1', async (request: Request, response: Response, next: NextFunction) => {
    console.log(`Loading the city version 1....`);

    let city: any = await new WeatherService().getCity();
    console.log(`current city : ${city.city}`);

    response.json({'info': city});
});

app.use('/city2', async (request: Request, response: Response, next: NextFunction) => {
    console.log(`Loading the city version 2....`);

    let city: any = await new WeatherService().getCityV2();
    console.log(`current city : ${city.city}`);

    response.json({'info': city});
});

app.use('/city3', async (request: Request, response: Response, next: NextFunction) => {
    console.log(`Loading the city version 3....`);

    let city: any = await new WeatherService().getCityV3();
    console.log(`current city : ${city.city}`);

    response.json({'info': city});
});

app.use('/city4', (request: Request, response: Response, next: NextFunction) => {
    console.log(`Loading the city version 4, error handling....`);

    new WeatherService().getCityV3WithErrorHanding().then((city: String) => {
        response.json({'info': city});
    }, (error: any) => {
        response.json(error);
    });


});

// catch 404 and forward to error handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {

    app.use((error: any, req, res, next) => {
        res.status(error['status'] || 500);
        res.render('error', {
            message: error.message,
            error
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((error: any, req, res, next) => {
    res.status(error['status'] || 500);
    res.render('error', {
        message: error.message,
        error: {}
    });
    return null;
});


export default app;