import * as rp from 'request-promise';

interface IWeatherResponse {
    title: String;
    location_type: String;
    City: String;
    woeid: String;
    latt_long: String;
}

export class WeatherService {

    private static URL: String = 'https://www.metaweather.com/api/location/search/?query=london';

    constructor() {
    }

    /**
     * version 1 is somewhat similar to your current implementation
     * @returns {Promise<T>}
     */
    public getCity(): Promise<any> {

        return new Promise((resolve, reject) => {
            let city: any = this.getWeather().then((result: IWeatherResponse) => {
                return WeatherService.extractCityInformation(result);
            }).catch((error) => {
                reject(error);
            });

            resolve(city);
        });
    }

    /**
     * version 2 gets rid of the promise
     * @returns {Promise<IWeatherResponse>}
     */
    public getCityV2(): Promise<any> {
        return this.getWeather().then((result: IWeatherResponse) => {
            return WeatherService.extractCityInformation(result);
        }).catch((error) => {
            console.error("error " + error);
        });
    }

    /**
     * version 3 uses the promise chaining to compact the code
     *
     * @returns {Promise<IWeatherResponse>}
     */
    public getCityV3(): Promise<any> {
        return this.getWeather()
            .then(WeatherService.extractCityInformation);
    }

    /**
     * version 4 is the same as version 3 but as a rejection handler
     *
     * @returns {Promise<IWeatherResponse>}
     */
    public getCityV3WithErrorHanding(): Promise<any> {
        return this.getWeather()
            .then(WeatherService.extractCityInformationFireError)
            .catch(WeatherService.rejectionHandler);
    }

    private static extractCityInformationFireError(): any {
        throw "error";
    }

    private static extractCityInformation(weatherInfo: IWeatherResponse): Promise<any> {

        return Promise.resolve({city: weatherInfo[0].title});
    }

    /**
     * used the request-promise so that I could convert the http call to a promise
     */
    private getWeather(): Promise<IWeatherResponse> {
        return rp({uri: WeatherService.URL, json: true});
    }

    /**
     *
     * @param error
     * @returns {Promise<never>}
     */
    private static rejectionHandler(error: any): any {
        return Promise.reject({type: "Error", message: "no connection..."});
    }
}